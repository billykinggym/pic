import base64
import hashlib
import tkinter as tk
import tkinter.messagebox
import pyperclip
from PIL import Image, ImageGrab, ImageTk

from const_config import FOLDER
from curl_util import updateFile, image2byte, year_month

clipImage:Image = None

def exit_program():
    exit(0)

def commit_program():
    global  clipImage
    # im = ImageGrab.grabclipboard()
    imgbytes = image2byte(clipImage)
    m = hashlib.md5()
    m.update(imgbytes)
    md5_str = m.hexdigest()

    data = base64.b64encode(imgbytes)
    yearmonth = year_month()
    file_path="%s/%s/%s.jpg"%(FOLDER,yearmonth, md5_str)
    v = updateFile(file_path, data.decode("utf-8"), "create file with md5 " + md5_str, operation="create")
    if v.startswith("http"):
        pyperclip.copy(v)
        tkinter.messagebox.showinfo("成功", "地址 %s 已经复制到剪贴板"% v)
    else:
        tkinter.messagebox.showinfo("错误", v)
    exit(0)


def main():
    txt_filename :tk.Entry

    def commit_program():
        global clipImage
        # im = ImageGrab.grabclipboard()
        imgbytes = image2byte(clipImage)
        md5_str = txt_filename.get()

        data = base64.b64encode(imgbytes)
        yearmonth = year_month()
        file_path = "%s/%s/%s.jpg" % (FOLDER, yearmonth, md5_str)
        v = updateFile(file_path, data.decode("utf-8"), "create file with md5 " + md5_str, operation="create")
        if v.startswith("http"):
            pyperclip.copy(v)
            tkinter.messagebox.showinfo("成功", "地址 %s 已经复制到剪贴板" % v)
        else:
            tkinter.messagebox.showinfo("错误", v)
        exit(0)

    window = tk.Tk()
    # 第2步，给窗口的可视化起名字
    window.title('My Window')
    # 第3步，设定窗口的大小(长 * 宽)
    window.geometry('800x800')  # 这里的乘是小x
    window.resizable(0,0)

    # 放置lable的方法有：1）l.pack(); 2)l.place();
    global clipImage
    clipImage = im = ImageGrab.grabclipboard()

    dstr = tk.StringVar()
    dstr.set("")
    pic_height=0
    if im is not None:
        m = hashlib.md5()
        imgbytes = image2byte(clipImage)
        m.update(imgbytes)
        md5_str = m.hexdigest()
        dstr.set(md5_str)
        image_file = ImageTk.PhotoImage(im)
        pic_height = im.height
        canvas = tk.Canvas(window, bg='white', height=im.height, width=im.width)
        image = canvas.create_image(0, 0, anchor='nw', image=image_file)
        canvas.grid(row=0,column=0,rowspan=2,columnspan=2,padx='4px', pady='5px')

    label = tk.Label(window,text="文件名")
    label.grid(row=2,column=0,sticky="w", padx='4px', pady='5px')

    txt_filename=tk.Entry(window,textvariable=dstr)
    txt_filename.grid(row=2,column=1, columnspan=5,sticky="w", padx='4px', pady='5px')


    commitBtn = tk.Button(window, text='提交', font=('Arial', 12), width=10, height=1, command=commit_program)
    commitBtn.grid(row=3,column=0,sticky="w", padx='4px', pady='5px')

    exitBtn = tk.Button(window, text='exit', font=('Arial', 12), width=10, height=1, command=exit_program)
    exitBtn.grid(row=3,column=1,sticky="w", padx='4px', pady='5px')

    # 第6步，主窗口循环显示
    window.mainloop()

if __name__ == '__main__':
    main()