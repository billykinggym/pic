import base64
import tkinter as tk
import tkinter.messagebox
import unittest
from datetime import datetime

from PIL import Image, ImageGrab, ImageTk

from curl_util import updateFile, image2byte, year_month


def hit_me():
    tkinter.messagebox.askquestion('提示', '要执行此操作吗')

class MyTest(unittest.TestCase):
    def test_date(self):
        print(year_month())

    def test_clip(self):
        im = ImageGrab.grabclipboard()
        print(base64.b64encode(image2byte(im)))

    def test_base64(self):
        print(base64.b64decode(b'YWJj').decode("utf-8"))  #


    def test_showWindow(self):
        window = tk.Tk()
        # 第2步，给窗口的可视化起名字
        window.title('My Window')
        # 第3步，设定窗口的大小(长 * 宽)
        window.geometry('500x300')  # 这里的乘是小x
        # 第4步，在图形界面上设定标签
        l = tk.Label(window, text='你好！this is Tkinter', bg='green', font=('Arial', 12), width=30, height=2)
        # 说明： bg为背景，font为字体，width为长，height为高，这里的长和高是字符的长和高，比如height=2,就是标签有2个字符这么高
        # 第5步，放置标签
        l.pack()  # Label内容content区域放置位置，自动调节尺寸
        # 放置lable的方法有：1）l.pack(); 2)l.place();

        im = ImageGrab.grabclipboard()
        image_file = ImageTk.PhotoImage(im)
        canvas = tk.Canvas(window, bg='green', height=im.height, width=im.width)
        image = canvas.create_image(0, 0, anchor='nw', image=image_file)
        canvas.pack()

        b = tk.Button(window, text='hit me', font=('Arial', 12), width=10, height=1, command=hit_me)
        b.pack()

        # 第6步，主窗口循环显示
        window.mainloop()

    def test_createFile(self):
        a= tkinter.messagebox.askquestion('提示', '要执行此操作吗')
        im = ImageGrab.grabclipboard()
        data = base64.b64encode(image2byte(im))
        print(updateFile("img/test.png", data.decode("utf-8"), "test",operation="create"))
        # print(base64.b64encode(b'abc'))
        # print(base64.b64decode(b'YWJj')) # b'binary\x00string')
