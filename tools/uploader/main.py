# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import sys
from time import sleep

from const_config import URL_PREIX
from curl_util import curl


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    file = ((sys.argv[1]))
    print(file+"\r\n")
    file.replace("D:\\soho_cloud\\pic\\", URL_PREIX)
    print(file)
    sleep(10000)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
