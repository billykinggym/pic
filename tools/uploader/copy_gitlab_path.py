# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import sys
import tkinter.messagebox
from time import sleep

import pyperclip

from const_config import URL_PREIX
from curl_util import curl


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # file like this D:\soho_cloud\pic\README.md
    file = ((sys.argv[1]))
    print(file+"\r\n")
    i = file.find("\\pic\\")
    if i <0:
        tkinter.messagebox.showinfo("失败", "没有找到pic文件夹")
        exit(0)
    file = URL_PREIX + file[i+len("\\pic\\"):]
    file = file.replace("\\", "/")
    print(file)
    pyperclip.copy(file)
    tkinter.messagebox.showinfo("成功", "地址 %s 已经复制到剪贴板" % file)





# See PyCharm help at https://www.jetbrains.com/help/pycharm/
