import io
import json
import subprocess
from datetime import datetime

from const_config import TOKEN, PROJECT_ID, URL_PREIX


def curl():
    output = subprocess.check_output(["curl", "--header","PRIVATE-TOKEN:  %s" % TOKEN,"https://gitlab.com/api/v4/projects/%s/repository/commits" % PROJECT_ID], shell=False)
    print(output)


def year_month():
    today = datetime.today()
    year = today.year
    month = today.month
    return str(year) + "/" + "{:0>2d}".format(month)

def image2byte(image):
    if image is None:
        return None
    '''
    图片转byte
    image: 必须是PIL格式
    image_bytes: 二进制
    '''
    # 创建一个字节流管道
    img_bytes = io.BytesIO()
    #把PNG格式转换成的四通道转成RGB的三通道，然后再保存成jpg格式
    image = image.convert("RGB")
    # 将图片数据存入字节流管道， format可以按照具体文件的格式填写
    image.save(img_bytes, format="JPEG")
    # 从字节流管道中获取二进制
    image_bytes = img_bytes.getvalue()
    return image_bytes

def updateFile(git_filepath, content_base64, commit_message, operation="create"):
    '''

    :param git_filepath:
    :param content_base64:
    :param commit_message:
    :param operation: create, delete, move, update, or chmod.
    :return:
    '''

    post_data={
        "branch" : "master",
        "commit_message": commit_message,
        "actions":[{"action":operation,"file_path":git_filepath,"content":content_base64,"encoding":"base64"}]
    }

    output = subprocess.check_output(["curl", "--request", "POST",
                                      "--header","PRIVATE-TOKEN:  %s" % TOKEN,
                                      "--header" , "Content-Type: application/json",
                                      "--data" ,  json.dumps(post_data) ,
                                      "https://gitlab.com/api/v4/projects/%s/repository/commits" % PROJECT_ID], shell=False)
    print(output)
    returnV = output.decode("utf-8")
    returnjson = json.loads(returnV)
    if "id" in returnjson:
        return URL_PREIX + git_filepath
    else:
        return "nok:"+ returnV
